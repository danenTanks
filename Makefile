all:
	mkdir -p bin/
	rm -rf bin/*
	javac -d bin/ src/game/*.java
	cp -r src/images bin/ 
	echo -e "#!/bin/bash\njava game/Tanks" > bin/play
#	ln -s bin/play play
	chmod 755 bin/play

clean:
	rm -rf bin
	rm -f play
