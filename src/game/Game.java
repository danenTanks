package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

@SuppressWarnings("serial")
public class Game extends JFrame implements KeyListener, Runnable
{
	private BufferStrategy bufferStrategy = null;
	private boolean gameRunning = true;
	
	private boolean keyUp = false, keyDown = false, keyLeft = false, keyRight = false;
	private boolean debugInfo = false;
	
	private Tank playerTank = new Tank();
	
	private long lastTime = System.nanoTime();
	
	public Game() 
	{
		this.setTitle("Tanks");
		this.setLayout(null);
		this.setIgnoreRepaint(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(new Dimension(800, 600));
		this.setLocation(300, 50);
		this.setResizable(false);
		this.addKeyListener(this);
		this.setVisible(true);
		
		// Try to make the system use hardware acceleration with Java2d
		System.setProperty("sun.java2d.transaccel", "True");
	    System.setProperty("sun.java2d.opengl", "True");
	    System.setProperty("sun.java2d.d3d", "True");
	    System.setProperty("sun.java2d.ddforcevram", "True");
	
		try {
			this.createBufferStrategy(2);
			bufferStrategy = this.getBufferStrategy();
		} catch (Exception e) {
			// try to recreate game.
			new Game();
			this.dispose();
		}
		
		// Start new thread for game loop
		new Thread(this).start();
	}
	
	public void run() 
	{
		Graphics2D g = null; // 2D graphics buffer
		
		Image tank = new ImageIcon("images/tank.png").getImage();
		
		playerTank.setImage(tank);
		playerTank.setSize(tank.getWidth(null), tank.getHeight(null));
		
		playerTank.move(200, 200);
		
		while (gameRunning)
		{
			g = (Graphics2D)bufferStrategy.getDrawGraphics();
			g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			
			// Clear background to black 
			g.setColor(Color.black);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
			
			// Update objects
			update();
			
			// Draw objects to screen
			render(g);
			
			// flush the G2D object and show the backbuffer
			g.dispose();
			bufferStrategy.show();
			
			// We sync display on linux systems
			Toolkit.getDefaultToolkit().sync();	
			try { Thread.sleep(20);	} catch (InterruptedException e) {}
            Thread.yield();
		}
	}
	
	/***
	 * Render all visible objects to screen
	 */
	private void render(Graphics2D g)
	{
		playerTank.draw(g);
		
		if (debugInfo == true) // Draw debug info
		{
			g.setColor(Color.white);
			g.drawString("DEG: " + Math.toDegrees(playerTank.getDirection()), 10, 50);
			g.drawString("RAD: " + playerTank.getDirection(), 10, 65);
		}
	}
	
	/***
	 * Update state of all entities. Handle user input
	 */
	private void update()
	{
		// Calculate how far objects should move
		long currentTime = System.nanoTime();
		double passedTime = (currentTime - lastTime) * 0.0000001;
		
		lastTime = currentTime;
		
		if (keyRight)
		{
			if (keyDown) // Change the way it rotates in reverse
				playerTank.rotate(-playerTank.getTurnSpeed() * passedTime);
			else
				playerTank.rotate(playerTank.getTurnSpeed() * passedTime);
		}
			
		if (keyLeft)
		{	
			if(keyDown)
				playerTank.rotate(playerTank.getTurnSpeed() * passedTime);
			else 
				playerTank.rotate(-playerTank.getTurnSpeed() * passedTime);
		}
			
		// Move tank forward
		if (keyUp) 
		{
			if (keyRight || keyLeft) // Slow tank
				playerTank.setSpeed(0.3);
			else
				playerTank.setSpeed(0.8);
			
			playerTank.move(-Math.sin(playerTank.getDirection()) * (passedTime * playerTank.getSpeed()), 
							Math.cos(playerTank.getDirection()) * (passedTime * playerTank.getSpeed()));
		}
		
		// Reverse tank
		if (keyDown) 
		{
			if (keyRight || keyLeft) // Slow tank
				playerTank.setSpeed(0.3);
			else
				playerTank.setSpeed(0.4);
				
			
			// Tank is slower in reverse (hence -speed)
			playerTank.move(Math.sin(playerTank.getDirection()) * (passedTime * playerTank.getSpeed()), 
							-Math.cos(playerTank.getDirection()) * (passedTime * playerTank.getSpeed()));
			
			// Reset speed to 1
			playerTank.setSpeed(1);
		}
		
		// Update all objects
		
	}

	public void keyPressed(KeyEvent e) 
	{
		switch (e.getKeyCode())
		{
			case KeyEvent.VK_W:
				keyUp = true;
				break;
				
			case KeyEvent.VK_S:
				keyDown = true;
				break;
				
			case KeyEvent.VK_D:
				keyRight = true;
				break;
				
			case KeyEvent.VK_A:
				keyLeft = true;
				break;
				
			case KeyEvent.VK_F1:
				debugInfo ^= true;
				break;
		}
	}


	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode())
		{
			case KeyEvent.VK_W:
				keyUp = false;
				break;
				
			case KeyEvent.VK_S:
				keyDown = false;
				break;
				
			case KeyEvent.VK_D:
				keyRight = false;
				break;
				
			case KeyEvent.VK_A:
				keyLeft = false;
				break;
		}
	}


	public void keyTyped(KeyEvent arg0) {}

}
