package game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

public class Tank 
{
	private double x, y, tankCenterX, tankCenterY;
	private double height = 0, width = 0;
	private double speed = 1, turnSpeed = 0.01;
	private double direction = 0.0;
	
	private AffineTransform matrix = null;
	private Image tankImage = null;
	
	public void draw(Graphics2D g) 
	{
		// Get center of tank using h and w
		tankCenterX = width/2;
		tankCenterY = height/2;
		
		matrix = g.getTransform(); // "push" the current matrix
		
		g.translate(x, y); // Move to the current tank location
		
		g.setColor(Color.cyan);
		g.drawString("Player 1", -10.0f, -10.0f);
		
		g.rotate(direction, tankCenterX, tankCenterY); // Rotate from the center of the tank

		g.drawImage(tankImage, 0, 0, null);
		
		g.setTransform(matrix); // "pop" the matrix back to the default
	}
	
	public void move(double xchange, double ychange)
	{
		x += xchange;
		y += ychange;
	}
	
	public void rotate(double amount)
	{
		direction += amount;
	}
	
	public double getDirection() {
		return direction;
	}
	
	public void setDirection(double d) {
		direction = d;
	}
	
	public void setSpeed(double newspeed) {
		speed = newspeed;
	}
	
	public double getSpeed() {
		return speed;
	}
	
	public double getTurnSpeed() {
		return turnSpeed;
	}
	
	public void setImage(Image img) {
		tankImage = img;
	}
	
	public void setSize(int w, int h) {
		width = w;
		height = h;
	}
}
